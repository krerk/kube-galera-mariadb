#!/bin/bash


set -eo pipefail
shopt -s nullglob

# if command starts with an option, prepend mysqld
if [ "${1:0:1}" = '-' ]; then
	set -- mysqld "$@"
fi

# skip setup if they want an option that stops mysqld
wantHelp=
for arg; do
	case "$arg" in
		-'?'|--help|--print-defaults|-V|--version)
			wantHelp=1
			break
			;;
	esac
done

# begin: added by krerk
if [ ! -f /tmp/is_init ]
then
echo Deverhood MariaDB Cluster configuration
echo =======================================
echo "Hostname - $HOSTNAME"

# set timezone
if [ ! -z "$DB_TIMEZONE" ]
then
echo "DB_TIMEZONE - $DB_TIMEZONE"
if [ -f /usr/share/zoneinfo/$DB_TIMEZONE ]
then
	rm /etc/localtime
	ln -s /usr/share/zoneinfo/$DB_TIMEZONE /etc/localtime
	echo system timezone is $DB_TIMEZONE
fi

#cat <<EOL > /docker-entrypoint-initdb.d/timezone.sql

#SET GLOBAL time_zone = '$DB_TIMEZONE';

#EOL

fi

# prepare config
master=`echo $HOSTNAME | cut -d- -f1`
seq=`echo  $HOSTNAME | cut -d- -f2`

if [ $seq = $master ]
then
	echo "  Cannot detect [host]-[seq]"
	echo "  Assume standalone server mode"
	
	echo "$@"
	exec docker-entrypoint.sh "$@"
	exit 0
fi

echo Master is $master-0.$master

echo Generating galera configuration
cat <<EOF > /etc/mysql/conf.d/galera.cnf
# galera configuration
[galera]
# Mandatory settings
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so 
binlog_format=row
default_storage_engine=InnoDB
innodb_autoinc_lock_mode=2
wsrep_provider_options="pc.recovery=TRUE;"
wsrep_cluster_address=gcomm://$master-0.$master,$master-1.$master,$master-2.$master

EOF

echo node seq - $seq
echo "init" > /tmp/is_init
fi
# end: added by krerk

# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

_check_config() {
	toRun=( "$@" --verbose --help --log-bin-index="$(mktemp -u)" )
	if ! errors="$("${toRun[@]}" 2>&1 >/dev/null)"; then
		cat >&2 <<-EOM

			ERROR: mysqld failed while attempting to check config
			command was: "${toRun[*]}"

			$errors
		EOM
		exit 1
	fi
}

# Fetch value from server config
# We use mysqld --verbose --help instead of my_print_defaults because the
# latter only show values present in config files, and not server defaults
_get_config() {
	local conf="$1"; shift
	"$@" --verbose --help --log-bin-index="$(mktemp -u)" 2>/dev/null | grep "^$conf " | awk '{ print $2 }'
}

# allow the container to be started with `--user`
if [ "$1" = 'mysqld' -a -z "$wantHelp" -a "$(id -u)" = '0' ]; then
	_check_config "$@"
	DATADIR="$(_get_config 'datadir' "$@")"
	if [ -d "$DATADIR/mysql" ] 
	then
		# this is exsiting node
		SAFE_BOOTSTRAP=`cat /var/lib/mysql/grastate.dat  |grep safe_to_bootstrap | cut -d\  -f2`
		if [ $SAFE_BOOTSTRAP == '1' ] && [ -d "$DATADIR/mysql" ] 
		then
			echo "Start mysqld with --wsrep-new-cluster to bootstrap existing cluster"
			exec gosu mysql "$@" --wsrep-new-cluster 
		else
			echo "Start mysqld without bootstrap "
			exec gosu mysql "$@"

		fi
	else
		# new node or secondary master
		mkdir -p "$DATADIR"
		chown -R mysql:mysql "$DATADIR"

		if [ $seq == '0' ]
		then
			echo "Start mysqld with --wsrep-new-cluster to bootstrap new cluster"
			exec gosu mysql /usr/local/bin/docker-entrypoint.sh "$@" --wsrep-new-cluster 
		else
			echo "Start mysqld (normal mode)"
			exec gosu mysql "$@"
		fi
	fi
fi

exec /docker-entrypoint.sh "$@"
