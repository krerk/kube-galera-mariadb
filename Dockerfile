FROM mariadb/server
LABEL maintainer="Krerk Piromsopa, Ph.D. <krerk.p@chula.ac.th>"

RUN apt-get update && apt-get install -y trickle

COPY --chown=mysql:mysql galera.cnf /etc/mysql/conf.d
RUN chmod 640 /etc/mysql/conf.d/galera.cnf
COPY cluster-entrypoint.sh /usr/local/bin/cluster-entrypoint.sh
RUN ln -s usr/local/bin/cluster-entrypoint.sh / # backwards compat
COPY mariadb-check.sh /usr/local/bin/mariadb-check.sh 
RUN chmod 755 /usr/local/bin/mariadb-check.sh
RUN ln -s usr/local/bin/mariadb-check.sh /
ENTRYPOINT ["cluster-entrypoint.sh"]

EXPOSE 3306
EXPOSE 4567
EXPOSE 4568
EXPOSE 4444

CMD ["mysqld"]
