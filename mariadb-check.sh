#!/bin/bash

echo "mariadb - ping"
ISALIVE=`mysqladmin ping 2>&1| grep alive`
echo $ISALIVE

if [ -z "$ISALIVE" ]
then
	echo "mariadb - is not alive."
	exit -1
else
	echo "mariadb - is alive."
	exit 0
fi
